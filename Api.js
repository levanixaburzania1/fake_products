import { generateProducts } from "./generateProducts.js";
import { SERVER__ADDR } from "./serverAddr.js";

let catalog = document.getElementById("catalog");

const request = fetch(SERVER__ADDR + "products")
  .then((response) => response.json())
  .then((products) => {
    let productList = "";
    for (const product of products) {
      productList += generateProducts(product);
    }
    catalog.innerHTML = productList;
  });

let input = document.getElementById("searchQuery");
let button = document.getElementById("search-button");

button.addEventListener("click", () =>
  fetch(SERVER__ADDR + "products")
    .then((response) => response.json())
    .then((products) => {
      let productList = "";
      for (const product of products) {
        if (product.title.includes(input.value)) {
          productList += generateProducts(product);
        }
      }
      catalog.innerHTML = productList;
    })
);

let sort = document.getElementById("sort");

sort.addEventListener("change", () => {
  let sortType = "?sort=" + sort.value;
  fetch(SERVER__ADDR + "products" + sortType)
    .then((response) => response.json())
    .then((products) => {
      let productList = "";
      for (const product of products) {
        if (product.title.includes(input.value)) {
          productList += generateProducts(product);
        }
      }
      catalog.innerHTML = productList;
    });
});
